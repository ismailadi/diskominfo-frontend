var instance = M.Dropdown.getInstance(elem);

    /* jQuery Method Calls
      You can still use the old jQuery plugin method calls.
      But you won't be able to access instance properties.

      $('.dropdown-trigger').dropdown('methodName');
      $('.dropdown-trigger').dropdown('methodName', paramName);
    */